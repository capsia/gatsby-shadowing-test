module.exports = {
  siteMetadata: {
    title: `My Gatsby Site`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-sass`
    },
    {
      resolve: '@mkitio/gatsby-theme-password-protect',
      options: {
        password: 'sUp3rS3cR3t' // delete or `undefined` to disable password protection
      }
    }
  ],
}
