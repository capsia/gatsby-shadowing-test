/**
 * Write-only the password as cookie
 */
import React, { useState } from 'react';
import { Button, TextInput } from '@carbon/react';
import { setSessionPassword } from '@mkitio/gatsby-theme-password-protect/src/utils/utils.js';

const styles = {
  wrapper: {
    height: '100vh',
    background: '#424242',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
};

const PasswordProtect = () => {
  const [password, setPassword] = useState('');

  const onSubmit = event => {
    event.preventDefault();
    setSessionPassword(password);
    window.location.reload(); // eslint-disable-line
  };

  return (
    <div style={styles.wrapper}>
      <h1 style={{ color: '#fff' }}>Hello world!</h1>
      <h4 style={{ color: '#fff' }}>Welcome to the magical world of shadowing :)</h4>

      <form onSubmit={onSubmit} style={{ width: '320px' }}>
        <TextInput.PasswordInput
          id="pwd"
          labelText="Password"
          type="password"
          value={password}
          onChange={event => setPassword(event.target.value)}
        />

        <Button type="submit">Enter</Button>
      </form>
    </div>
  );
};

export default PasswordProtect;
